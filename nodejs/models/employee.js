const mongoose = require('mongoose');



var Employee = mongoose.model('Employees', {
    FirstName: { type: String },
    LastName: { type: String },
    Desgination: { type: String },
    Emailid : { type: String },
    Phoneno : { type: Number },
    AddressLine1 : { type: String },
    AddressLine2 : { type: String },
    AddressLine3 : { type: String },
    city : { type: String },
    State : { type: String },
    Pincode : { type: Number }
});

module.exports = { Employee };
