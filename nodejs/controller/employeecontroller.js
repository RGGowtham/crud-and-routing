const express = require('express');
var router = express.Router();
var Objectid = require('mongoose').Types.ObjectId;

var {Employee} = require('../models/employee');

router.get('/', (req, res) => {
    Employee.find((err, docs) => {
        if(!err) {res.send(docs);}
        else{console.log('Error in Retariving EmployeeData :' +  JSON.stringify(err, undefined, 2)); }
        
    });
});

router.get('/:id',(req,res) => {
    if(!Objectid.isValid(req.params.id))
        return res.status(400).send(`No record with givenid : ${req.params.id}`);
    Employee.findById(req.params.id,(err,doc) => {
        if(!err) { res.send(doc); }
        else { console.log('Error in Retriving Employee Id :' + JSON.stringify(err,undefined,2)); }
    });
});

router.post('/',(req,res) => {
    var empData = new Employee ({
    FirstName: req.body.FirstName,
    LastName: req.body.LastName,
    Desgination: req.body.Desgination,
    Emailid : req.body.Emailid,
    Phoneno : req.body.Phoneno,
    AddressLine1 : req.body.AddressLine1,
    AddressLine2 : req.body.AddressLine2,
    AddressLine3 : req.body.AddressLine3,
    city : req.body.city,
    State : req.body.State,
    Pincode : req.body.Pincode
    });
    empData.save((err, doc) => {
        if(!err){ res.send(doc); }
        else {console.log('Error in Saving EmployeeData :' + JSON.stringify(err,undefined,2));}
    });

});

router.put('/:id',(req,res) => {
    if(!Objectid.isValid(req.params.id))
    return res.status(400).send(`No Record With Given Id : ${req.params.id}`);

    var empData={
    FirstName: req.body.FirstName,
    LastName: req.body.LastName,
    Desgination: req.body.Desgination,
    Emailid : req.body.Emailid,
    Phoneno : req.body.Phoneno,
    AddressLine1 : req.body.AddressLine1,
    AddressLine2 : req.body.AddressLine2,
    AddressLine3 : req.body.AddressLine3,
    city : req.body.city,
    State : req.body.State,
    Pincode : req.body.Pincode
    };
    Employee.findByIdAndUpdate(req.params.id,{ $set: empData }, { new : true }, (err,doc) => {
        if(!err) { res.send(doc); }
        else { console.log("Error in Upating Employee Data : " + JSON.stringify(err,undefined,2)); }

    });
})

router.delete('/:id',(req,res) => {
    if(!Objectid.isValid(req.params.id))
    return res.status(400).send(`No Record With Given Id : ${req.params.id}`);

    Employee.findByIdAndRemove(req.params.id, (err,doc) => {
        if(!err) { res.send(doc); }
        else { console.log('Error in Deleting EmployeeData : ' + JSON.stringify(err,undefined,2)); }
    });
});

module.exports = router;