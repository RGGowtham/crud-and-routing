import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-box',
  templateUrl: './login-box.component.html',
  styleUrls: ['./login-box.component.css']
})
export class LoginBoxComponent implements OnInit {
 name : any;
  constructor(private router : Router ) {     
  }

  ngOnInit() {
  }
    onForRegClick(){
      this.router.navigate(['./userregistration']);
    }
    onForLoginClick(){
      this.router.navigate(['./employeehomepage']);
    }
}
