import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataBaseViewComponent } from './data-base-view.component';

describe('DataBaseViewComponent', () => {
  let component: DataBaseViewComponent;
  let fixture: ComponentFixture<DataBaseViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataBaseViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataBaseViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
