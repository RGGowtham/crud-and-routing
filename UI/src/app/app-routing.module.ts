import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeComponent } from './employee/employee.component';
import { LoginBoxComponent } from './login-box/login-box.component';
import { DataBaseViewComponent } from './data-base-view/data-base-view.component';
import { UserRegistrationComponent } from './user-registration/user-registration.component';
import { ProfilepageComponent } from './profilepage/profilepage.component';


const routes: Routes = [
  { path : '', component : LoginBoxComponent},
  { path : 'userregistration', component : UserRegistrationComponent},
  { path : 'employeehomepage', component : ProfilepageComponent} ,
  { path : 'profileeditpage', component : EmployeeComponent},
  { path : 'employeedataview', component : DataBaseViewComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
