import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profilepage',
  templateUrl: './profilepage.component.html',
  styleUrls: ['./profilepage.component.css']
})
export class ProfilepageComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  onNavProfileEditPage(){
    this.router.navigate(['./employeedataview']);
  }
 
}
